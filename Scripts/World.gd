extends Node2D

onready var player = $Player;
onready var resetPoint = $Environment/ResetPoint;

func _process(delta) -> void:
	if(player.global_position.y >= resetPoint.global_position.y):
		print("Reset");
		get_tree().reload_current_scene();

func loadNextScene():
	print("Loading next scene");
	pass
