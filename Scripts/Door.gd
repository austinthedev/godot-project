extends Node2D

var unlocked: bool = false;
onready var doorSprite: AnimatedSprite = $AnimatedSprite;
onready var staticBody: StaticBody2D = $StaticBody2D;
onready var world: Node2D = get_tree().get_root().get_node("World");

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Check if player has key
func _on_Area2D_body_entered(body):
	print("Player Body entered");
	if(!unlocked && body.is_in_group("Player")):
		var player = body.get_parent();
		print("Has item: ", player.hasItem());
		print("Is key: ", player.getCurrentItem().is_in_group("Key") );
		if(player.hasItem() && player.getCurrentItem().is_in_group("Key")):
			print("Door unlocked");
			unlocked = true;
			doorSprite.frame = 0;
			remove_child(staticBody);
			player.getCurrentItem().destroy();
		else:
			print("Door locked");
