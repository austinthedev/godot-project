extends Node2D

var inRange: bool = false;
var offset: Vector2 = Vector2(0, -50);

enum PropState { IDLE = 0, HELD };
var currenState = PropState.IDLE;

onready var player: Node2D = get_tree().get_current_scene().get_node("Player");
onready var rb: RigidBody2D = $"RigidBody2D";

func _ready():
	rb.set_as_toplevel(true);

func _process(delta: float):
	if(inRange):
		if(player.isGrabbing && !player.hasItem()):
			currenState = PropState.HELD;
	if(!player.isGrabbing):
		currenState = PropState.IDLE;
	
	match(currenState):
		PropState.HELD:
			player.setCurrentItem(self);
			player.setHasItem(true);
			rb.followPos = self.global_position;
			rb.isFollowing = true;
		PropState.IDLE:
			player.setHasItem(false);
			rb.isFollowing = false;

func _physics_process(delta: float):
	match(currenState):
		PropState.HELD:
			self.global_position = player.global_position + offset;
		PropState.IDLE:
			self.global_position = self.global_position.linear_interpolate(rb.global_position, delta * 20);
			
func destroy():
	print("Destroyed: ", self.get_groups());
	player.setHasItem(false);
	player.setCurrentItem(null);
	self.queue_free();
