extends Node2D

# Child Nodes
onready var moveTarget = $KinematicBody2D;
onready var animSprite = $Sprites/AnimatedSprite;
onready var animTree = $Sprites/AnimationTree;
onready var stateMachine = $Sprites/AnimationTree.get("parameters/playback");

# Exported Values
export var moveSpeed : float = 800;
export var jumpForce: float = 700;
export var gravityMultiplier: float = 5;

var hInput : float;
var velocity : Vector2;

enum CreatureMode { SHIFTER = 0, SQUID, BAT };
enum State { NULL = -1, IDLE, WALKING, JUMPING };
var currentCreature = CreatureMode.SHIFTER;
var currentState = State.IDLE;

# Arial variables
var isGrounded: bool = false;
var gravityForce: float = 0;
var jumpAmount: float = 0;
var _jumpBufferCounter : float = -1;
var _hangtimeCounter : float = -1;
var jumpBuffer: float = 0.1;
var hangtime: float = 0.1;
var cancelJump: bool;
var cancelBuffer: bool;
var maxJumps: int = 1;
var jumpsLeft: int = 0;
var jumpPressed: bool = false;

var holdingItem: bool = false;
var isGrabbing: bool = false;
var _currentItem: Node2D = null;

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Makes the move target's position global 
	moveTarget.set_as_toplevel(true);
	animTree.set_active(true);

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	match(currentState):
		State.NULL:
			return;
		State.IDLE:
			stateMachine.travel("Idle");
			pass;
		State.WALKING:
			stateMachine.travel("Run");
			pass;
		State.JUMPING:
			stateMachine.travel("Jump");
			pass;
	
	self.position = self.position.linear_interpolate(moveTarget.position, delta * 20);
	
	if(velocity.x == 0 && isGrounded):
		currentState = State.IDLE;
		#stateMachine.travel("Idle");
	elif( velocity.x != 0 && isGrounded):
		currentState = State.WALKING
		#stateMachine.travel("Run");
	
	if(velocity.x < 0):
		animSprite.set_flip_h( true );
	elif(velocity.x > 0):
		animSprite.set_flip_h( false );
		
	if(isGrounded):
		_hangtimeCounter = hangtime;
	else:
		_hangtimeCounter -= delta;
		if(velocity.y < 0):
			currentState = State.JUMPING;
		
	self.handleInputs(delta);
	animSprite.rotation_degrees = hInput * 10;

func _physics_process(delta: float) -> void:
	
	match(currentState):
		State.NULL:
			return;
		State.IDLE:
			pass;
		State.WALKING:
			pass;
		State.JUMPING:
			pass;
	
	# Check Ground collision 
	# Ground check handled by Area2D Node
	# _hangtimeCounter >= 0
	if( _jumpBufferCounter >= 0 && jumpsLeft > 0 && jumpPressed):
		position.y -= 0.1;
		moveTarget.position.y -= 0.1;
		jumpAmount = -jumpForce;
		velocity.y = jumpAmount;
		gravityForce = 0;
		jumpPressed = false;
	
	if(false && cancelJump && cancelBuffer && gravityForce < jumpForce):
		print("No");
		jumpAmount = -gravityForce;
		cancelJump = false;
		cancelBuffer = false;
	
	velocity.x = hInput * moveSpeed;
	velocity.y += gravityForce;
	
	velocity.y = clamp( velocity.y, -1000, 1000 );
	
	moveTarget.move_and_slide( velocity, Vector2.UP, true );
	
	if(isGrounded):
		gravityForce = 0.001;
		jumpAmount = 0;
		jumpsLeft = maxJumps;
		cancelBuffer = true;
	else:
		gravityForce += 10 * gravityMultiplier;
		
func handleInputs(delta: float):
	
	if(Input.is_key_pressed(KEY_1)):
		currentCreature = CreatureMode.SHIFTER;
		maxJumps = 1;
		jumpsLeft = 0 if jumpsLeft > maxJumps else jumpsLeft;
	elif(Input.is_key_pressed(KEY_2)):
		currentCreature = CreatureMode.SQUID;
		maxJumps = 0;
		jumpsLeft = 0 if jumpsLeft > maxJumps else jumpsLeft;
	elif(Input.is_key_pressed(KEY_3)):
		maxJumps = 10;
		currentCreature = CreatureMode.BAT;
		
	animSprite.frame = currentCreature;
	
	if(Input.is_action_just_pressed("ui_accept")):
		jumpsLeft -= 1;
		_jumpBufferCounter = jumpBuffer;
		#stateMachine.travel("Jump");
		jumpPressed = true;
	else:
		_jumpBufferCounter -= delta;
	
	if(Input.is_action_just_released("ui_accept")):
		cancelJump = true;
	
	hInput = (Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"));
	
	match(currentCreature):
		CreatureMode.SHIFTER:
			if(Input.is_action_just_pressed("shift")):
				print("Grabbing");
				isGrabbing = true;
			if(Input.is_action_just_released("shift")):
				print("Letting go");
				isGrabbing = false;
		CreatureMode.BAT:
			isGrabbing = false;
		CreatureMode.SQUID:
			isGrabbing = false;
	
	if(!isGrabbing):
		self.setCurrentItem(null);
	
	if(self.hasItem()):
		print("HEREEE");

func hasItem():
	return holdingItem;
	
func setHasItem(value : bool):
	holdingItem = value;

func setCurrentItem(item: Node2D):
	if(self.hasItem()):
		return;
	_currentItem = item;
	
func getCurrentItem():
	return _currentItem;

func _on_Area2D_body_entered(body):
	print("Grounded from Player");
	self.isGrounded = true;
	self.velocity.y = 0;

func _on_Area2D_body_exited(body):
	print("Airborn from Player");
	self.isGrounded = false;

# Prop in range
func _on_Area2D_area_entered(area):
	print("Prop in range");
	area.get_parent().inRange = true;

# Prop out of range
func _on_Area2D_area_exited(area):
	print("Prop out of range");
	area.get_parent().inRange = false;
