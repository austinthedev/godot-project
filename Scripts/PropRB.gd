extends RigidBody2D

var isFollowing = false;
var followPos: Vector2 = Vector2.ZERO;

func _integrate_forces(state):
	if(isFollowing):
		var originalTransform = state.get_transform();
		originalTransform.origin = self.followPos;
		state.set_transform(originalTransform);
		state.set_angular_velocity(0);
		state.set_linear_velocity(Vector2.ZERO);
