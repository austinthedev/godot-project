extends Camera2D

onready var camTarget = get_parent().get_node("CamTarget");

export var followSpeed: float = 10;

# Called when the node enters the scene tree for the first time.
func _ready():
	self.set_as_toplevel(true);
	self.position = camTarget.global_position;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.position = self.position.linear_interpolate(camTarget.global_position, delta * followSpeed);
